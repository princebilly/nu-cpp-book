#include<iostream>

using namespace std;

int main(){
	int a=3,b=4;
	int const *p=&a; //same as 
	int *const q=&a;
	const int *r=&a; //same as int const *r
	//const *int= *r=&a //not correct
	
	 p=&b;
	 cout<<*p<<endl;
	 //we can change the address that int const *p it is initialized to.
	 // *p=100; //gives error of assigning read-only location
	 // we cannot change the contents that int const *p points to.
	 
	 //q= &b; //give error of assigning read only varriable
	 //cannot change the address that int *const q is initialized to.
	 *q=100; //We can change the contents that int *const q points to
	 cout<<*q<<endl;
	 
	 r=&b;
	 cout<<*r<<endl;
	 //*r=100; //error 
	 //same behaviour as int const *p
	
	return 0;
}
